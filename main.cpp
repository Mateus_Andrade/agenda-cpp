#include <iostream>
#include "pessoa.hpp"
#include "amigos.hpp"
#include "contatos.hpp"

using namespace std;

// main file
int main () {
	
	Contatos primeiroContato;
	Amigos primeiroAmigo;

	cout << "\nNovo Contato:\n" << endl;
		
// Antes da entrada dos dados de primeiroContato

	cout << "Nome: " << primeiroContato.getNome() << endl;
	cout << "Idade: " << primeiroContato.getIdade() << endl;
	cout << "Telefone: " << primeiroContato.getTelefone() << endl;
	cout << "Telefone Comercial: " << primeiroContato.getTelComercial() << endl;
	cout << "Endereço: " << primeiroContato.getEndereco() << endl;

// Entrada de dados de primeiroContato

	primeiroContato.setNome("Fulano da Silva");
	primeiroContato.setIdade("45");
	primeiroContato.setTelefone("555-5555");
	primeiroContato.setTelComercial("444-4444");
	primeiroContato.setEndereco("404 SCS ED. VARGAS Brasilia-DF");
	
// Depois da entrada dos dados de primeiroContato

	cout << "\nNome: " << primeiroContato.getNome() << endl;
        cout << "Idade: " << primeiroContato.getIdade() << endl;
	cout << "Telefone: " << primeiroContato.getTelefone() << endl;
	cout << "Telefone Comercial: " << primeiroContato.getTelComercial() << endl;
	cout << "Endereço: " << primeiroContato.getEndereco() << endl;

// fim primeiroContato
	
	cout << "\nNovo Amigo:\n" << endl;

// Antes da entrada dos dados de primeiroAmigo

	cout << "Nome: " << primeiroAmigo.getNome() << endl;
	cout << "Idade: " << primeiroAmigo.getIdade() << endl;
	cout << "Telefone: " << primeiroAmigo.getTelefone() << endl;
	cout << "Apelido: " << primeiroAmigo.getApelido() << endl;
	cout << "Facebook: " << primeiroAmigo.getFacebook() << endl;

// Entrada de dados de primeiroAmigo

	primeiroAmigo.setNome("Beltrano");
	primeiroAmigo.setIdade("38");
	primeiroAmigo.setTelefone("333-3333");
	primeiroAmigo.setApelido("Bel");
	primeiroAmigo.setFacebook("http://facebook.com/profile=1234");

// Depois da entrada dos dados de primeiroAmigo

	cout << "\nNome: " << primeiroAmigo.getNome() << endl;
        cout << "Idade: " << primeiroAmigo.getIdade() << endl;
        cout << "Telefone: " << primeiroAmigo.getTelefone() << endl;
        cout << "Apelido: " << primeiroAmigo.getApelido() << endl;
        cout << "Facebook: " << primeiroAmigo.getFacebook() << endl;

	return 0;
}
