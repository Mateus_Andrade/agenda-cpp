all:		pessoa.o contatos.o amigos.o main.o
		g++ -o main pessoa.o main.o contatos.o amigos.o

pessoa.o:	pessoa.cpp pessoa.hpp
		g++ -c pessoa.cpp

contatos.o:	contatos.cpp contatos.hpp
		g++ -c contatos.cpp

amigos.o:	amigos.cpp amigos.hpp
		g++ -c amigos.cpp

main.o:		main.cpp pessoa.hpp contatos.cpp amigos.cpp
		g++ -c main.cpp

clean:
		rm -rf *.o

run:
		./main
