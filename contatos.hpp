#ifndef CONTATO_H
#define CONTATO_H

#include "pessoa.hpp"

class Contatos : public Pessoa{
	private:
		string telcomercial;
		string endereco;
	public:
		Contatos();
		Contatos(string telcomercial, string endereco);
		string getTelComercial();
		void setTelComercial(string telcomercial);
		string getEndereco();
		void setEndereco(string endereco);
};

#endif
