#ifndef AMIGOS_H
#define AMIGOS_H

#include "pessoa.hpp"

class Amigos : public Pessoa{
	private:
		string apelido;
		string facebook;
	public:
		Amigos();
		Amigos(string apelido, string facebook);
		string getApelido();
		void setApelido(string apelido);
		string getFacebook();
		void setFacebook(string facebook);
};

#endif
