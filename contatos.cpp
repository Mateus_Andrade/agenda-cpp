#include "contatos.hpp"

using namespace std;

Contatos::Contatos(){
	setTelComercial("...");
	setEndereco("...");
}

Contatos::Contatos(string telcomercial, string endereco){
	setTelComercial(telcomercial);
	setEndereco(endereco);
}

string Contatos::getTelComercial(){
	return telcomercial;
}

void Contatos::setTelComercial(string telcomercial){
	this->telcomercial=telcomercial;
}

string Contatos::getEndereco(){
	return endereco;
}

void Contatos::setEndereco(string endereco){
	this->endereco=endereco;
}
