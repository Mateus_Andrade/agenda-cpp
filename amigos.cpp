#include "amigos.hpp"

using namespace std;

Amigos::Amigos(){
	setApelido("...");
	setFacebook("...");
}

Amigos::Amigos(string apelido, string facebook){
	setApelido(apelido);
	setFacebook(facebook);
}

string Amigos::getApelido(){
	return apelido;
}

void Amigos::setApelido(string apelido){
	this->apelido=apelido;
}

string Amigos::getFacebook(){
	return facebook;
}

void Amigos::setFacebook(string facebook){
	this->facebook=facebook;
}
